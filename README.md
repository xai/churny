# README #

### What is this repository for? ###

Just a small code churn analysis tool

### How do I get set up? ###

As a requirement, libgit2 has to be installed. There are packages for
many popular distributions, otherwise have a look at
http://libgit2.github.com.

Currently supported are versions 0.21 and 0.22.

If you have libgit2 installed, just run `make` to compile a binary. Then
run `./churny -h` to print information on its usage.

Note: there is also a bash script in this repository that also
calculates code churn, but is no longer updated.

### Who do I talk to? ###

Olaf Lessenich (lessenic@fim.uni-passau.de)
